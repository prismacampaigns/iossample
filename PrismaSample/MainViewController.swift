//
//  ViewController.swift
//  Test2
//
//  Created by Alejandro Winkler on 9/11/17.
//  Copyright © 2017 Alejandro Winkler. All rights reserved.
//

import UIKit
import Prisma

class MainViewController: UIViewController, PrismaRedirectHandler {
    
    @IBOutlet weak var lblResult: UILabel!
    @IBOutlet weak var ph: Placeholder!

    func onRedirect(url: URL) -> Bool {
        self.present(WevViewController(url: url), animated: true, completion: nil)
        return true
    }
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    override func viewWillAppear(_ animated: Bool) {
        /* aquí se está sincronizando un único placeholder (el que se agregó en la UI con el nombre "MegaNewApp"),
         * pero podría ser una lista de placeholders (de hecho se está pasando como parámetro una lista con un único elemento).
         */
        Client.shared!.syncView(
            viewName: "Main",
            placeholders: [ph],
            onSynced: {(response: Dictionary<String, PlaceholderContent>) -> Bool in
                for  ph in response {
                    /* en la respuesta se recibe la lista de banners encontrada para los placeholders enviados como
                     * parámetro en la solicitud. Hay que recorrerlos y hacer lo que se necesite para el caso de cada app.
                     */
                    if (ph.key == "CarruselAppNova") { // en este ejemplo nos quedamos únicamente con el placeholder que queremos.
                        for banner in ph.value.banners {
                            /* para cada placeholder puede haber más de un banner, por lo tanto la respuesta nos trae una lista.
                             * Tenemos que recorrer la lista y hacer lo que se necesite para el caso de cada app.
                             */
                            self.lblResult.text = (banner as! HtmlBannerView).getHTMLContent()
                        }
                    }
                }
                return true
            }
        )
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        Client.shared?.setRedirectHandler(redirectHandler: self)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
}

