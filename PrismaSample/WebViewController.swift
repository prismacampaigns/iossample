//
//  WebViewController.swift
//  Test2
//
//  Created by Alejandro Winkler on 25/8/18.
//  Copyright © 2018 Alejandro Winkler. All rights reserved.
//

import UIKit
import WebKit

class WevViewController: UIViewController, WKUIDelegate, WKNavigationDelegate {
    
    var webView: WKWebView!
    var url: URL?
    
    override open var shouldAutorotate: Bool {
        return false
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
    init(url: URL) {
        self.url = url
        super.init(nibName: nil, bundle: nil)
    }
    
    override func loadView() {
        let webConfiguration = WKWebViewConfiguration()
        webView = WKWebView(frame: .zero, configuration: webConfiguration)
        webView.uiDelegate = self
        webView.navigationDelegate = self
        view = webView
    }
    
    func webView(_ webView: WKWebView, decidePolicyFor navigationAction: WKNavigationAction, decisionHandler: @escaping (WKNavigationActionPolicy) -> Void) {
        if (navigationAction.request.url?.absoluteString.starts(with: "http://192.168.1.106/redpagos/?cupon="))! {
            self.dismiss(animated: true, completion: {
              decisionHandler(.cancel)
            })
        } else {
            decisionHandler(.allow)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        webView.load(URLRequest(url:self.url!))
    }
}
